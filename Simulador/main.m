%Programa

%Iniciar figura de ploteo
figure('Name','Visualizaci�n drones','NumberTitle','off','OuterPosition',[1 1 10^5 10^5 ]);

%Condiciones_acabar_simulacion
tic
ntoc=1; %Inicializa para los guardados
tocguardar=60*1; %enminutos
tocmaximo=3600*20; %en horas

%Inicio la k de almacenaje
k=1;


while toc<tocmaximo
    
 for N1=2:12
     for N2=2:10
         for N3=1:4
 if toc<tocmaximo       
     
%Probabilidad
z1=0.8+rand*1.2;
z2=z1+rand*(2-z1);
zb=(1/z1-1/z2)/15;
za=1/z2-10*zb;
alfa=rand*pi/3;

    
    
    
%% Cargar datos
env=Environment;
env.rd=rand*20;
env.r0=500+300*rand;





%Decisiones
decisionstop=false;
tipodebalas=1;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             


%Datos ataque
attack=2;


%Datos de las propiedades del sistema de ataque defensivo
numberofshots=ceil(rand*8);
reloadtime=10*rand; %en segundos



%Parametros temporales
dt=0.05;
tf=5000;
t=0;

stop=false; %Inicialmente no est� parado, luego veremos si se para.

%Formacion defensiva
N=[N1 N2 N3]; %Numero de atacantes por fila
alcance2=[1 N2 1]*1; %Multiplicador del alcance (1 es hasta el drone contiguo)
[Xform,env.s]=formacion(N,[1 1 1],env.zopt); %Saco numero de drones defensivos

if env.s<=75 %Condicion que limita las simulaciones a un numero de drones defensivos
%Defino drones enemigos
env.t=ceil(env.s*(0.5+3*rand));
tadicional=zeros(1,env.s);
tiempoview=zeros(1,env.s);

%Disparos restantes y tiempo de recarga, inicializaci�n y probabilidad de fallo
Number=ones(1,env.s)*numberofshots;
treload=zeros(1,env.s);
probabilidad_fallo=0.7+rand*0.3;


%Posici�n inicial
env.fmax=[500*rand 500*rand 100*rand]; %Defino el tama�o del ataque
env.B=ones(1,env.t); %Inicio la matriz de drones eliminados
[env.Xt,env.Xs,env.Xsg]=condini(env);
[Xtg,SIGMAINICIAL,theta]=propertiest(env);
env.Xs(:,3)=Xtg(3);


%Velocidades del drone
Vx=15+10*rand;
env.Vs=Vx;
env.Vt=Vx*(0.5+rand);
Vasc=5+2*rand;
Vdesc=Vasc*2/3;
dt2=0.1/Vx;



%Distribuir la defensa en funci�n del enemigo de forma inicial %A partir de aqui defino la posici�n predefinida inicial (en cada paso tempral se har� lo mismo)
separacion=[sqrt(3),sqrt(3),sqrt(3)]*2.*SIGMAINICIAL./(N); %Separacion
cerca=separacion<env.separacionminima;
separacion(cerca)=env.separacionminima(cerca);
[Xform,env.s]=formacion(N,separacion,env.zopt);
R=[cos(theta) sin(theta) 0;-sin(theta) cos(theta) 0;0 0 1];    
for i=1:env.s
    Xs(i,:)=env.Xsg+Xform(i,:)*R;
end
env.Xs=Xs;

%Vectores que definen si estan eliminados y asignaciones
A=zeros(1,env.s)*NaN;
env.B=ones(1,env.t);
D=ones(1,env.t);
C=zeros(env.t,env.s)*NaN; 

%Contador para el ploteo
tplot=0;

%Condici�n para el bucle temporal
condition=true;
%coinciden=false; %Bucle de coincidencias (Obviamente se supone que no al principio)




%% Funci�n para el seguimiento    (Se ha eliminado esta forma de resolverlo al ser poco eficiente)
% syms x y z Vx Vy Vz ts r xr yr Vm xf yf zf a b c
% f='a^2+b^2+c^2=Vm^2';
% xf=(x+Vx*ts);
% yf=(y+Vy*ts);
% zf=(z+Vz*ts);
% xr=r*xf/sqrt(xf^2+yf^2);
% yr=r*yf/sqrt(xf^2+yf^2);
% 
% a=(xf-xr)/ts;
% b=(yf+yr)/ts;
% c=(zf/ts);
% 
% f=subs(f);
% 
% sol=solve(f,ts);

%% Bucle temporal

while condition

    %Extraer las propiedades de los atacantes 
    [Xtg,SIGMA,theta]=propertiest(env);
    Xr=rel(env,theta,Xs);
    
    %Distribuir la defensa en funci�n del enemigo
    separacion=[sqrt(6),sqrt(6),sqrt(6)].*SIGMA./(N-1*(N>1)); %Separacion
    cerca=separacion<env.separacionminima;
    separacion(cerca)=env.separacionminima(cerca);
    Xform=formacion(N,separacion,env.zopt);
    alcance=separacion.*alcance2;
    recorridomaximo=1000000*norm(alcance(1:2));
    
    
        
    %% Sacar velocidad de atacantes
    
    if attack~=0      %Ataques que buscan una posici�n
        Xt=stept(env.Xt,t,attack);
        Vt=(Xt-env.Xt)/dt;

        for i=1:env.t
        Vt(i,:)=controlvelocidad(Vt(i,:),env.Vt);
        end
    else             %Ataque hacia abajo
        Vt(:,:)=0;
        Vt(:,2)=-env.Vt;
    end
    
    %% Sacar velocidad  de defensores por formaci�n
    %Saca movimiento del centro
    R=[cos(theta) sin(theta) 0;-sin(theta) cos(theta) 0;0 0 1];
    if ~stop
    Xtg_objetivo=Xtg-[0 SIGMA(2)*sqrt(6)/2 0]*R;
    Vsg=(Xtg_objetivo-env.Xsg)*100;
    Vsg=controlvelocidad(Vsg,env.Vs);
    Xsg=env.Xsg+dt*Vsg;
    end
    
    % Distribuye la formaci�n a partir el centro
    for i=1:env.s
    Xs(i,:)=Xsg+Xform(i,:)*R;
    end
    
    %Asigna la velocidad predefinida por formaci�n
    Vs=Xs-env.Xs;
    for i=1:env.s
    Vs(i,:)=controlvelocidad(Vs(i,:),env.Vs);
    end
    
    
    
    %% Asignacion de objetivos
    C(:,:)=NaN;
    for i=1:env.s
        if ~(A(i) > 0) & Number(i)>0 & treload(i)<=0; %Que no tenga nada asociado ya, que le queden disparos y que no est� recargando
            for j=1:env.t
                if D(j)==1 & all(A~=j)
                %Introduzco las propiedades para decirle si est� o no dentro del area de influencia  (Con las posiciones relativas recogidas en Xr (referidas a la formaci�n)) 
                dentro= abs(Xr(j,i,1))<alcance(1);
                dentro= dentro & abs(Xr(j,i,2))<alcance(2);
                dentro= dentro & abs(Xr(j,i,3))<alcance(3);
                   if dentro
                       dt=dt2;
                       C(j,i)=norm(env.Xt(j,:)-env.Xs(i,:)); %-[0; 0 ;env.zopt]);   %Aqu� se recoge la distancia del drone defensivo i al atacante j (si es un posible objetivo).
                                                                               %La preferencia de asignaci�n vendr� por la cercan�a real al drone no a la poici�n te�rica.
                   end 
                end
            end
        end
    end
    
    
    
%     (Se ha eliminado esto al ser poco eficiente)
%     %Una vez extraida la matriz de los que estan dentro del rango, hago una asignaci�n preliminar mediante las distancias recogidas en la matriz C.
%     
%     for i=1:env.s
%             if A(i) == 0 
%                 [minimo pos]=min(C(:,i));
%                 if minimo>=0
%                     A(i)=pos;
%                 end
%             end   
%         end
%         
%      %Miro a ver si hay coincidentes y lo arreglo      
%      while coinciden  
%         [C,A,coinciden]=coincidentes(A,C);
%      end


%     [M,I] = min(A(:),nanflag)
%     [I_row, I_col] = ind2sub(size(A),I)

 %Buscamos minimos de distancia y los asociamos al que tiene que atacar mirando las distancias contenidas en C
  while any(any(C))
           [minimo,position] = min(C(:));%Mirar que sea una distancia finita (No NaN)
           [j,i]=ind2sub(size(C),position);
           
           
           %Mirar que no est� muy lejos si no no asignarlo
           [talcance,posible]=seguimiento3(env.Xs(i,:),env.Xt(j,:),Vx,Vasc,Vdesc,eta,Vt(j,:),env.rd);
           %[talcance,val,flag]=fzero(@(tvar) seguimientozero(tvar,env.Xs(i,:),env.Xt(j,:),Vx,Vasc,Vdesc,eta,Vt(j,:),env.rd),dt);
           tiempoview(i)=talcance;
           if posible
                 Xf=(env.Xt(j,:)-env.Xs(i,:))+Vt(j,:)*talcance;
                 Xfproyectado=Xf.*[1 1 0];
                 Dseguimiento=(Xf-env.rd*(Xfproyectado)./norm(Xfproyectado));
                 if norm(Dseguimiento(1:2))<recorridomaximo
                     A(i)=j;
                     C(:,i)=NaN;
                     C(j,:)=NaN;
                 else
                     C(j,i)=NaN;
                 end 
           else
               C(j,i)=NaN;
           end
  end

           
           
           
  

  
    %% Velocidades con asignaci�n de objetivo
    
    eta=1; %La velocidad no puede ser tan alta y asignamos unas p�rdidas a aceleraciones y dem�s
    
    
    for i=1:env.s
        if A(i)>=0
          [talcance,posible]=seguimiento3(env.Xs(i,:),env.Xt(A(i),:),Vx,Vasc,Vdesc,eta,Vt(A(i),:),env.rd); %Saco el tiempo resuelto
          %[talcance,val,flag]=fzero(@(t)seguimientozero(t,env.Xs(i,:),env.Xt(A(i),:),Vx,Vasc,Vdesc,eta,Vt(A(i),:),env.rd),dt);
          tiempoview(i)=talcance;
          
          if posible
          Xf=(env.Xt(A(i),:)-env.Xs(i,:))+Vt(A(i),:)*talcance;
          Xfproyectado=Xf.*[1 1 0];
          Vs(i,:)=(Xf-env.rd*(Xfproyectado)./norm(Xfproyectado))/talcance;
          
          
          % Disparar

              if talcance<dt*1.1

                  Number(i)=Number(i)-1;
     
                  unitario=env.Xt(A(i),:)-env.Xs(i,:);
                  unitario=unitario(1:2)/norm(unitario(1:2));
                  normal=[unitario*cos(alfa) sin(alfa)];
                  vperp=norm(cross(normal,Vt(A(i),:)));
                  r=za+zb*vperp;
                  z=1/r;
                  if abs(randn) < z %Nos dice si falla el disparo
                     env.B(A(i))=0;
                     env.Xt(A(i),:)=0;
                  end
                  A(i)=NaN;
              end
              
          end
        end
        
   
    end
    
 
    %% Sacar el nuevo estado
    env.Xt=env.Xt+dt*Vt;
    env.Xs=env.Xs+dt*Vs;
    env.Xsg=Xsg;
    
    
    

     %% Plotear
     if tplot==100;
     pintar(env)
     tplot=0;
     end
     tplot=tplot+1;
    
    %% Finalizar bucle
    t=t+dt;
    condition = t<tf  & ~(all((env.Xt(:,1:2).^2)*[1;1]<env.r0^2)) & ~(all(env.B==0));
    D((env.Xt(:,1:2).^2)*[1;1]<env.r0^2)=0;
    treload=treload-dt;
    
    
end


%% Almacenar datos
 Nx(k,1)=N(1);
 Ny(k,1)=N(2);
 Nz(k,1)=N(3);
 Dispersionx(k,1)=SIGMAINICIAL(1);
 Dispersiony(k,1)=SIGMAINICIAL(2);
 Dispersionz(k,1)=SIGMAINICIAL(3);
 Drones_atacantes(k,1)=env.t;
 Drones_defensivos(k,1)=env.s;
 Drones_restantes(k,1)=sum(env.B);
 Tiempo(k,1)=t;
 Formacion{k,1}='Cuadrado';
 Formacion_numero(k,1)=1;
 Probabilidad_acierto(k,1)=probabilidad_fallo;
 Radio_ataque(k,1)=env.rd;
 Radio_base(k,1)=env.r0;
 Radio_deteccion(k,1)=env.ra;
 Disparos(k,1)=numberofshots;
 T_recarga(k,1)=reloadtime;
 V_enemigo(k,1)=env.Vt;
 V_crucero(k,1)=Vx;
 V_ascenso(k,1)=Vasc;
 Z1(k,1)=z1;
 Z2(k,1)=z2;
 Alfa(k,1)=alfa;
 k=k+1;
 
 
 %Almacenar la tabla
 if toc>tocguardar*ntoc
        ntoc=ntoc+1;
        %% Tabla
        Tabla=table(Nx,Ny,Nz,Dispersionx,Dispersiony,Dispersionz,Drones_atacantes,Drones_defensivos,Drones_restantes,Tiempo,Formacion,Formacion_numero,Radio_ataque,Radio_base,Radio_deteccion,Disparos,T_recarga,V_enemigo,V_crucero,V_ascenso,Z1,Z2,Alfa);
        writetable(Tabla,'Tabla_Cuadrado_3_Uni.txt')  
end
 
end
 end

 
 


         end
     end
 end


end
Tabla=table(Nx,Ny,Nz,Dispersionx,Dispersiony,Dispersionz,Drones_atacantes,Drones_defensivos,Drones_restantes,Tiempo,Formacion,Formacion_numero,Radio_ataque,Radio_base,Radio_deteccion,Disparos,T_recarga,V_enemigo,V_crucero,V_ascenso,Z1,Z2,Alfa);
writetable(Tabla,'Tabla_Cuadrado_3_Uni.txt')  
