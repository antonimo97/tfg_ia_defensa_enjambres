classdef Environment
   properties
       %En m
       r0=1300/2;
       ra=5000;
       rd=0;
       
       %Drones
       Vt=8;
       Vs=5;
       delay=1; %En segundos
       zopt=0;
       s; %N�mero de defensores
       t=30; %N�mero de atacantes
       Xt;
       Xs;
       Xsg;
       Xtg;
       
       %Informaci�n para decisiones
       Xr;   %Tabla de distancias entre drones
       
       %Formacion
       formacion=1;   %1 Cuadrado %2 triangulo %3 en rombo %4 Esfera
       flecha=0; %flecha en grado
       separacionminima=[1 1 5];
       
       %Formaci�n inicial ataque
       fmax=[1000 1000 200];
       attackinicial=1;
       
       %Matriz de eliminaci�n de drones
       B;

 
       
   end
     methods
      
      %% Funci�n para la condici�n inicial
         
      function [Xt,Xs,Xsg] = condini(obj) %defino localizaci�n inicial de los drones
          
          %Atacantes
          
          if obj.attackinicial==0
              theta=linspace(105,75,obj.t)*pi/180;
              Xt=[obj.ra*cos(theta);obj.ra*sin(theta)]';
              Xt(:,3)=0;
          elseif obj.attackinicial==1
              for i=1:obj.t
              Xt(i,:)=[0 obj.ra 0]+((rand(1,3))*2-1).*obj.fmax;
              end
          end
          
          %Defensores
          Xs=zeros(obj.s,3);
          Xs(:,3)=0;
          Xsg=[0,0,0];
         
      end
      
      %% Propiedades de atacantes
      
      function [Xtg,SIGMA,theta]=propertiest(obj)
         Xtg=obj.B*obj.Xt;
         Xtg=Xtg/sum(obj.B);
         theta=atan2(Xtg(2),Xtg(1))-pi/2;
         R=[cos(theta),-sin(theta); sin(theta) cos(theta)];
         n=size(obj.Xt);
         for i=1:n(1)
         Xt2(i,:)=R*obj.Xt(i,1:2)';
         end
         Xtg2=R*Xtg(1:2)';
         SIGMA=([sum((Xt2(:,1)-Xtg2(1)).^2.*obj.B'),sum((Xt2(:,2)-Xtg2(2)).^2.*obj.B'),sum((obj.Xt(:,3)-Xtg(3)).^2.*obj.B')]/sum(obj.B)).^(0.5);

      end
      
      %% Centro de gravedad
     function Xsg= centrogravedad(obj)
         Xsg=[sum(obj.Xs(:,1)),sum(obj.Xs(:,2)),sum(obj.Xs(:,3))]/(obj.s);
     end
      
      %% Funci�n de ploteo
     
      
      function pintar(obj)
            figure(1)
            clf
            hold on
            
            %Circulo defensivo
%             viscircles([0,0],obj.ra,'Color','k','LineStyle','--');
%             filledCircle([0,0],obj.r0,100,[0 1 1]);
            
            %Drones atacantes (Rojos)
            X=obj.Xt(:,1);
            Y=obj.Xt(:,2);
            Z=obj.Xt(:,3);
            
            
            scatter3(X,Y,Z,'r')
            limitx=obj.ra*[-0.6 0.6];
            limity=obj.ra*[0 1.05];
            xlim(limitx)
            ylim(limity)
            
            %Drones defensores
            X=obj.Xs(:,1);
            Y=obj.Xs(:,2);
            Z=obj.Xs(:,3);
            
            
            %Drones defensivos(azules)
            scatter3(X,Y,Z,'g')
            view(90,0)
      end
      
      
      
      %% Obtener posiciones relativas para toma de decisiones
      
      function Xr=rel(obj,theta,Xs)
          Xr=zeros(obj.t,obj.s,3);
          R=[cos(theta) sin(theta) 0;-sin(theta) cos(theta) 0;0 0 1];
          for i=1:obj.t
              for j=1:obj.s
                  Xr(i,j,:)=[obj.Xt(i,:)-Xs(j,:)]*R'-[0 0 obj.zopt];
              end
          end
          
      end
      
    end
end