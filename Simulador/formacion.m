function [X,s]=formacion(N,separacion,zopt)

%Cuadrada
    for i=1:N(1)
        for j=1:N(2)
            for k=1:N(3)
                n=i+(j-1)*N(1)+(k-1)*N(1)*N(2);
                X(n,:)=([i,j,k]-[1,1,1]-(N-1).*0.5).*separacion+[0 0 zopt];
            end
        end
    end
    s=n;


end