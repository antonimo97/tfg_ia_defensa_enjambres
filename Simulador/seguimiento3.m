function [t,solucionado]=seguimiento3(Xs,Xt,Vx,Vasc,Vdesc,eta,Vt,r)
epsilon=0.05;
Xr=Xt-Xs;
solucionado=false;
Vx=eta*Vx;
Vasc=Vasc*eta;
Vdesc=Vdesc*eta;
t=0;
maxiter=30;


%Sacar los tiempos de cambio de funci�n problem�ticos
d=-Xr(1:2)*Vt(1:2)'/(norm(Vt(1:2)));
r1=sqrt(norm(Xr)^2-d^2);
if r1>r
    tp=[0,d,2000]/(norm(Vt(1:2)));
    n=2;
else
    r2=sqrt(r^2-r1^2);
    if d-r2>0
    tp=[0,d-r2,d,d+r2,2000]/(norm(Vt(1:2)));
    n=4;
    else
    tp=[0,d,d+r2,2000]/(norm(Vt(1:2)));
    n=3;
    end
    
end

if Vt(3)*Xr(3)<0
    n=n+1;
    tp(n+1)=-Xr(3)/Vt(3);
    sort(tp);
end

%Tengo el tiemo asi que resuelvo los 4 intervalos

%Resuelvo la ecuaci�n
for i=1:n
    iter=0;
    if  ~solucionado & iter<maxiter
        %Saco a y b
        a=tp(i)+10^(-7);
        b=tp(i)+0.01;
        xfa=Xr+Vt*a;
        xfb=Xr+Vt*b;
        Va=xfa-r*[xfa(1) xfa(2) 0]/norm(xfa(1:2));
        Vb=xfb-r*[xfb(1) xfb(2) 0]/norm(xfb(1:2));
        gammaa=atan(Va(3)/norm(Va(1:2)));
        gammab=atan(Vb(3)/norm(Vb(1:2)));
        if gammaa>0
                Vz=Vasc;
            else
                Vz=Vdesc;
            end

        gammaa=atan(Vx/Vz*tan(gammaa));    
        Vma=sqrt((Vz*sin(gammaa))^2+(Vx*cos(gammaa))^2);
        if gammab>0
                Vz=Vasc;
            else
                Vz=Vdesc;
            end
        gammab=atan(Vx/Vz*tan(gammab)); 
        Vmb=sqrt((Vz*sin(gammab))^2+(Vx*cos(gammab))^2);
        fa=norm(Va)-Vma*a;
        fb=norm(Vb)-Vmb*b;
        
        
        %Saco d y e
        d=tp(i+1)-0.01;
        e=tp(i+1)-10^(-7);
        xfa=Xr+Vt*d;
        xfb=Xr+Vt*e;
        Va=xfa-r*[xfa(1) xfa(2) 0]/norm(xfa(1:2));
        Vb=xfb-r*[xfb(1) xfb(2) 0]/norm(xfb(1:2)); 

        gammaa=atan(Va(3)/norm(Va(1:2)));    
        gammab=atan(Vb(3)/norm(Vb(1:2)));
        if gammaa>0
                Vz=Vasc;
            else
                Vz=Vdesc;
            end

        gammaa=atan(Vx/Vz*tan(gammaa));    
        Vma=sqrt((Vz*sin(gammaa))^2+(Vx*cos(gammaa))^2);
        if gammab>0
                Vz=Vasc;
            else
                Vz=Vdesc;
            end
        gammab=atan(Vx/Vz*tan(gammab)); 
        Vmb=sqrt((Vz*sin(gammab))^2+(Vx*cos(gammab))^2);
        fd=norm(Va)-Vma*d;
        fe=norm(Vb)-Vmb*e;
        
        
        

        
        %Primer caso, cuando tiene 1 solucion
        if fe<0 %Caso en el que tiene 1 solucion
            while ~solucionado & iter<maxiter
                c=a+(a-b)*fa/(fb-fa);
                xfc=Xr+Vt*c;
                Vc=xfc-r*[xfc(1) xfc(2) 0]/norm(xfc(1:2));
                gammac=atan(Vc(3)/norm(Vc(1:2)));
                if gammac>0
                        Vz=Vasc;
                    else
                        Vz=Vdesc;
                    end
                gammac=atan(Vx/Vz*tan(gammac)); 
                Vmc=sqrt((Vz*sin(gammac))^2+(Vx*cos(gammac))^2);
                fc=norm(Vc)-Vmc*c;
                if abs(fc)<epsilon
                    solucionado=true;
                else
                    a=b;
                    b=c;
                    fa=fb;
                    fb=fc;
                end
                iter=iter+1;
            end
            
        else if fe>fd %2Soluciones posibles
               %Desarrollo algoritmo para ver si tiene o no solucion con
               %una caida desde el punto inicial hasta el m�nimo
               A=[b-a ,d-e;fb-fa,fd-fe];
               C=[d-a;fd-fa];
               Lambda=A\C;
               
               fh=fa+(fb-fa)*Lambda(1);
               
               if fh<0  %Podr�a haber soluci�n asi que vamos a mirarlo
               
                   while ~solucionado & iter<maxiter
                    c=a+0.01;
                    xfc=Xr+Vt*c;
                    Vc=xfc-r*[xfc(1) xfc(2) 0]/norm(xfc(1:2));
                    gammac=atan(Vc(3)/norm(Vc(1:2)));
                        if gammac>0
                                Vz=Vasc;
                            else
                                Vz=Vdesc;
                        end
                    gammac=atan(Vx/Vz*tan(gammac)); 
                    Vmc=sqrt((Vz*sin(gammac))^2+(Vx*cos(gammac))^2);
                    fc=norm(Vc)-Vmc*c;
                        if abs(fc)<epsilon
                            solucionado=true;
                        elseif abs(fc)>abs(fb) | c<b
                            %logic=false;
                        else
                            a=b;
                            b=c;
                            fa=fb;
                            fb=fc;
                        end
                        iter=iter+1;
                   end
               end
               
               
            end
   
        end
    end
    
    if solucionado
        t=c;
    end
    
end
end